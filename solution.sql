-- 1. Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE '%d%';

 -- 2. Find all songs that has a length of less than 3:50.
SELECT * FROM songs WHERE length < 350;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.
SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.album_id;


--4.  Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.

SELECT artists.id, artists.name, albums.* FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE '%a%';

--5. Sort the albums in Z-A order. (Show only the first 4 records.

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--6. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

SELECT albums.id, albums.album_title, albums.date_released, albums.artist_id, songs.id, songs.song_name, songs.length, songs.genre, songs.album_id
	FROM albums 
	JOIN songs ON albums.id = songs.album_id 
	ORDER BY albums.album_title DESC, songs.song_name ASC;